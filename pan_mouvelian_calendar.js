function normal_to_mouvelian(date_value) {
    return date_value - 687;
}

var MONTHS = new Array("Janvier", "Février", "Mars", "Avril", "Mai", "Juin",
"Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre");

function get_mouvelian_season_format(absolute_day) {
    if(absolute_day >= 0 && absolute_day < 90) {
        return (absolute_day+1) + " Zéphyr";
    } else if(absolute_day >= 90 && absolute_day < 180) {
        return (absolute_day+1-90) + " Phénix";
    } else if(absolute_day >= 180 && absolute_day < 270) {
        return (absolute_day+1-180) + " Scion";
    } else if(absolute_day >= 270 && absolute_day < 360) {
        return (absolute_day+1-270) + " Colosse";
    } else {
        return (absolute_day+1-360) + " Hivernel";
    }
}

function enrich_title(calendar, date_value) {
    var original_title = calendar.find("h1[class='entry-title']");
    original_title.after(
        "<h2 class='mouvelian_year'>" + date_value + " après l'Exode</h2>"
    );
}

function day_of_year(day, month, year) {
    var now = new Date(year, month, day);
    var start = new Date(year, 0, 0);
    var diff = now - start;
    var oneDay = 1000 * 60 * 60 * 24;
    return Math.floor(diff / oneDay);
}

function get_context_data(calendar) {
    var month_year_regexp = /([^ ]+), ([0-9]+)/;
    var original_title = calendar.find("h1[class='entry-title']");
    var matches = month_year_regexp.exec(original_title.text());
    
    return new Array(MONTHS.indexOf(matches[1]), parseInt(matches[2]));
}

function get_day_number(calendar_day) {
    return parseInt($.trim(calendar_day.text()));
}

function enrich_calendar(context, calendar_days) {
    // Handle special case for first day
    var day_number = get_day_number(calendar_days[0]);
    var year = context[1];
    if(day_number == 1) {
        var month = context[0];
    } else {
        var month = context[0] - 1;
        if(month < 0) {
            --year;
            month = 11;
        }
    }
    var absolute_day = day_of_year(day_number-1, month, year);
    var mouvelian_season = get_mouvelian_season_format(absolute_day);
    
    calendar_days[0].append(
        "<div class='mouvelian_day'>" + mouvelian_season + "</div>"
    );
    
    for(var i = 1; i < calendar_days.length; ++i) {
        day_number = get_day_number(calendar_days[i]);
        if(day_number < get_day_number(calendar_days[i-1])) {
            ++month;
            if(month > 11) {
                month = 0;
                ++year;
            }
        }
        
        absolute_day = day_of_year(day_number-1, month, year);
        mouvelian_season = get_mouvelian_season_format(absolute_day);
        calendar_days[i].append(
            "<div class='mouvelian_day'>" + mouvelian_season + "</div>"
        );
    }
    
}


$(document).ready(function() {
    var calendar = $("#calendar");

    var context = get_context_data(calendar);

    enrich_title(calendar, normal_to_mouvelian(context[1]));

    var calendar_days_objs = new Array();
    var calendar_days = calendar.find("div[class='calendar-day']")
        .add("div[class='calendar-day-wkend']")
        .add("div[class='calendar-day-current']").each(
        function(index) {
            calendar_days_objs.push($(this));
        }
    );

    enrich_calendar(context, calendar_days_objs);
});